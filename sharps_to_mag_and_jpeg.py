import os
import csv
import glob
import shutil
import pandas as pd
from astropy.io import fits
from sunpy.io.header import FileHeader

#for changing to jpeg
import numpy as np
import sunpy.map
import matplotlib.pyplot as plt

from time import time
import multiprocessing as mp

import datetime


def process_files(foldername):

    folderpath = os.path.join(main_dir, foldername)
    files_inside_folder = listdir_nohidden(folderpath)

    magnetogram_files = [file for file in files_inside_folder if 'magnetogram' in file]

    for magnetogram_file in magnetogram_files:
        folder = magnetogram_file.split("/")[4]
        filename = magnetogram_file.split("/")[5]
        systime = magnetogram_file.split(".")[3]
        date = systime.split("_")[0]
        time = systime.split("_")[1]

        if time in ['000000', '060000', '120000', '180000']:
            hdul = fits.open(magnetogram_file)
            hdr = hdul[1].header 
            try:
                LONDTMIN, LONDTMAX = hdr['LONDTMIN'], hdr['LONDTMAX']

                if LONDTMIN >= -70 and LONDTMAX <= 70:

                    dest_folder_fits = str(os.path.join(destination_for_fits, str(folder)))

                    if not os.path.isdir(dest_folder_fits):
                        os.mkdir(dest_folder_fits)

                    destination_fitsfile_path = os.path.join(dest_folder_fits, filename)
                    shutil.copy(magnetogram_file, destination_fitsfile_path)
                    

                    # for jpeg
                    hmi = sunpy.map.Map(magnetogram_file)  
                    img = plt.imshow(np.clip(hmi.data,-1500,1500), cmap="hmimag", vmax=1500, vmin=-1500)
                    plt.axis(False)
                                        
                    dest_folder_jpeg = str(os.path.join(destination_for_jpeg, str(folder)))

                    if not os.path.isdir(dest_folder_jpeg):
                        os.mkdir(dest_folder_jpeg)

                    destination_jpegfile_path = os.path.join(dest_folder_jpeg, filename.replace(".fits",".jpg"))
                    plt.savefig(destination_jpegfile_path, bbox_inches='tight',pad_inches=0)


            except:
                pass




harpnum_dir = "/home/sshrestha8/Project/SolarFlareClassification/allharp2noaa2.csv"
main_dir = "/data/SHARPS/raw-sharps"
destination_for_fits = "/data/Magnetograms/SIX_HOURLY/FITS_NEW"
destination_for_jpeg = "/data/Magnetograms/SIX_HOURLY/JPEG_NEW"

folder_list = os.listdir(main_dir)
df = pd.read_csv(harpnum_dir)
harpnum_list = df["DEF_HARPNUM"].tolist()

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))

def listdir_nohidden(path):
    return glob.glob(os.path.join(path, '*'))

common_folder_list = intersection(folder_list, list(map(str, harpnum_list))) #2796
destination_folder_list = os.listdir(destination_for_fits)


common_folder_list = list(set(common_folder_list) - set(destination_folder_list)) # this id second time so omit folders which have already been placed

pool = mp.Pool(110)

results = pool.map(process_files, common_folder_list)

pool.close()


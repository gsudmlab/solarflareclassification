#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=200gb
#SBATCH --time=2-00:00:00
#SBATCH --mail-type=END,BEGIN,FAIL
#SBATCH --mail-user=sshrestha8@student.gsu.edu
#SBATCH --account=csc344s73
#SBATCH --partition=qGPU48
#SBATCH --gres=gpu:V100:2
#SBATCH --output=outputs/output_%j
#SBATCH --error=errors/error_%j

cd /scratch
mkdir $SLURM_JOB_ID
cd $SLURM_JOB_ID
iget /arctic/projects/csc344s73/fds/harp/solarflareclassification_googlenet.py
iget -r /arctic/projects/csc344s73/fds/harp/labelling
iget -r /arctic/projects/csc344s73/fds/harp/label_source
iget -r /arctic/projects/csc344s73/fds/harp/models
iget -r /arctic/projects/csc344s73/fds/harp/checkpoint

source /home/users/sshrestha8/environment/super_resolution/bin/activate
python solarflareclassification_googlenet.py
cd /scratch

iput -rf $SLURM_JOB_ID